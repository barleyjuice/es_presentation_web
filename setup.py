from distutils.extension import Extension
from setuptools import setup, find_namespace_packages

setup(
    name='es_presentation_web',
    version='0.1.0',
    author='Aliona Petrova',
    author_email='consciencee95@gmail.com',
    packages=['es_presentation.web'],
    install_requires=[
        'Flask',
        'Flask-SocketIO',
    ],
    url='https://bitbucket.org/barleyjuice/es_presentation_web/',
    description='Web presentation plugin for Experiment Suite Core',
    long_description=open('README.md').read(),
    include_package_data=True
)
