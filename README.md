# Web presentation plugin for Experiment Suite Core #

Allows present stimuli (currently for P300-like schemes with imagese only) in web UI. Module includes built-in Flask web-server to run on main thread within Experiment Suite Core Application.

The stimuli are prepared externally (this package doesn't contain any built-in stimuli, except for fixation cross image), placed in the special folder, and the absolute path of this folder must be passed as input parameter on presentation instance creation.

## Installation ##

To install the module as dependency in your Python project simply type:
```
pip install git+https://barleyjuice@bitbucket.org/barleyjuice/es_presentation_web.git#egg=es_presentation_web
```
If the installation was successful, you will be able to import the module in your project:
```python
from es_presentation.web import WebPresenter
```

## Use ##

Firstly create `WebPresenter` object and pass in constructor `port`, on which web-server will be deployed, and `stimuli_folder`, where your stimuli images are stored:

```python
stimuli_folder = os.path.join(os.getcwd(), 'images', 'emotions')

presenter = WebPresenter(port=8081, stimuli_folder=stimuli_folder)
```
Then the above constructed `WebPresenter` object in `presenter` variable can be passed as input parameter to create `Application` instance:

```python
# assume that `experimentScheme` and `recorder` are constructed avove
app = Application(
    presenter=presenter, 
    experimentScheme=experimentScheme, 
    recorder=recorder)
```

Now when `app.run()` will be called, the Flask server will start on localhost and specified port.