let stimuliData = {};
let experimentWorkflow = {
    sequence: []
};

let cancel_session = false;

document.addEventListener('keydown', function(event) {
    if (event.ctrlKey && event.key === 'x') {
        cancel_session = true;
        endSession();
    }
});

function startSession() {
    document.getElementById('status').innerText = '';
    document.getElementById('start-btt').type = 'hidden';
    let subjectId = document.getElementById('subject-id').value;
    fetch(`/start?id=${subjectId}`)
        .then(() => {
            hideSidePanel();
            console.log("ok");
            run(experimentWorkflow);
        })
        .catch(err => alert(err));

}

function stopSession() {
    unsetStimuli();
    fetch(`/stop`).then(() => console.log("stopped")).catch(err => alert(err));
}

function getMeta() {
    fetch('/experiment-meta').
    then(resp => resp.json()).
    then(data => {
            stimuliData = data.stimuli.reduce((acc, stim) => {
                let { id, src, category } = stim;
                src = `/stim/${src}`
                acc[id] = { src, category, id };
                new Image().src = src; // cache
                return acc;
            }, {});
        })
        .catch(err => alert(err));
}

function getWorkflowMeta() {
    fetch('/experiment-workflow').
    then(resp => resp.json()).
    then(data => {
            let { sequence, interStimulusTime, stimulusTime, initialFixationTime } = data.workflow;
            experimentWorkflow = {
                sequence,
                interStimulusTime,
                stimulusTime,
                initialFixationTime
            }
        })
        .catch(err => alert(err));
}

function reportOnset(id, category) {
    fetch(`/onset?id=${id}&category=${category}`).
    then(() => console.log("onset transmitted")).
    catch(err => alert(err));
}

function reportUnset() {
    fetch(`/unset`).
    then(() => console.log("unset transmitted")).
    catch(err => alert(err));
}

function reportEnd() {
    fetch(`/end`).
    then(() => console.log("end transmitted")).
    catch(err => alert(err));
}

function startListening() {
    const socket = io();
    socket.on("connect", () => {
        console.log("connected");
    });

    socket.on("data", (data) => {
        processEvent(data.data);
    });

    socket.on("status", (status) => {
        processEvent(status.data);
    });

    socket.on("connect_error", () => {
        console.log("connect error");
    });

    socket.on("disconnect", () => {
        console.log("disconnected");
    });
}

function processEvent(evtData) {
    switch (evtData.type) {
        case 'onset':
            onsetStimuli(evtData.stimulus);
            break;
        case 'unset':
            unsetStimuli();
            break;
        case 'end':
            endSession();
            break;
        default:
            console.error(`Unexpected event: ${evtData.type}`);
    }
}

function hideSidePanel() {
    document.getElementById("side-panel-container").style.display = "none";
}

function showSidePanel() {
    document.getElementById("side-panel-container").style.display = "block";
}

function getCurrentTimestamp() {
    return Date.now();
}

function onsetStimuli(stimuliInfo) {
    if (cancel_session) {
        return;
    }
    console.log(`onset ${stimuliInfo.id}, src ${stimuliInfo.src} category ${stimuliInfo.category} timestamp ${String(getCurrentTimestamp())}`);
    document.getElementById('stimuli-placement').src = stimuliData[stimuliInfo.id].src;
    reportOnset(stimuliInfo.id, stimuliInfo.category);
}

function unsetStimuli() {
    if (cancel_session) {
        return;
    }
    console.log('unset');
    showFixationCross();
    reportUnset();
}

function showFixationCross() {
    document.getElementById('stimuli-placement').src = 'cross.png';
}

function timeout(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function promisify(fn) {
    return new Promise((resolve, reject) => {
        if (cancel_session) {
            reject("canceled");
        }
        try {
            resolve(fn());
        } catch (e) {
            reject(e);
        }
    })
}

function stimulusFlow(stimuli, stimulusTime, interStimulusTime) {
    if (interStimulusTime) {
        return promisify(() => onsetStimuli(stimuli))
            .then(() => timeout(stimulusTime * 1000))
            .then(() => promisify(unsetStimuli))
            .then(() => timeout(interStimulusTime * 1000));
    } else {
        return promisify(() => onsetStimuli(stimuli))
            .then(() => timeout(stimulusTime * 1000))
            .then(() => promisify(unsetStimuli));
    }

}

function initialFixationCross(initialFixationTime) {
    return promisify(() => showFixationCross())
        .then(() => timeout(initialFixationTime * 1000))
}

async function run({ sequence, stimulusTime, interStimulusTime, initialFixationTime }) {
    console.log(`stimulus time, s: ${stimulusTime}, inter-stimulus time, s: ${interStimulusTime}`)
    let sessionFlow = Promise.resolve();
    await initialFixationCross(initialFixationTime);
    sequence.map(stimuli => {
        sessionFlow = sessionFlow.then(() => stimulusFlow(stimuli, stimulusTime, interStimulusTime));
    });
    await sessionFlow;
    endSession();
}

function endSession() {
    reportEnd();
    showSidePanel();
    document.getElementById('stimuli-placement').src = '';
    document.getElementById('status').innerText = 'Session ended!';
}