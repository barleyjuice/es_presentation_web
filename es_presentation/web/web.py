from .flaskapp import create_application
# from gevent import pywsgi
# from geventwebsocket.handler import WebSocketHandler
class WebPresenter:
    def __init__(self, port, stimuli_folder) -> None:
        self.port = port
        self.stimuli_folder = stimuli_folder

    def set_application_parameters(self, params_dict):
        self.params = params_dict

    def set_start_presentation_calee(self, calee):
        self.start_presentation_calee = calee

    def set_stop_presentation_calee(self, calee):
        self.stop_presentation_calee = calee

    def set_event_source(self, event_source):
        self.event_source = event_source

    def set_change_id_calee(self, calee):
        self.id_calee = calee

    def run(self):
        print("Attempting to run flask app")
        self.run_app = create_application("experiments_app", self.params["experiment_scheme"], \
            self.start_presentation_calee, self.stop_presentation_calee, self.id_calee, self.event_source, self.stimuli_folder)(self.port)
        # self.server = pywsgi.WSGIServer(('0.0.0.0', self.port), self.run_app, handler_class=WebSocketHandler)
        # self.server.serve_forever()

