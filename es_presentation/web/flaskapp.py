from flask import Flask, render_template, Response, request, send_from_directory
from flask_socketio import SocketIO, send, emit
import os

def create_application(app_name, experiment_scheme, start_handler, stop_handler, id_handler, event_source, stimuli_folder=''):

    flask_dir = os.path.dirname(__file__)
    template_path = os.path.join(flask_dir, "static")

    if stimuli_folder == '':
        stimuli_folder = template_path

    app = Flask(app_name, template_folder=template_path, static_url_path='', static_folder=template_path)
    async_mode = None
    sockets = SocketIO(app, async_mode=async_mode)

    @app.route("/")
    def welcome():
        return render_template('index.html', scheme=experiment_scheme)

    @app.route("/start")
    def begin():
        id_handler(request.args.get('id'))
        start_handler()
        return "ok"

    @app.route("/stop")
    def stop():
        stop_handler()
        return "ok"

    @app.route("/stim/<path:filename>")
    def stim(filename):
        return send_from_directory(stimuli_folder, filename)

    @app.route("/experiment-meta")
    def meta():
        return {'stimuli': event_source.get_stimuli_set()}

    @app.route("/experiment-workflow")
    def workflow_meta():
        return {'workflow': event_source.get_workflow_meta()}

    @app.route("/unset")
    def unset():
        event_source.emit_unset()
        return "ok"

    @app.route("/onset")
    def onset():
        event_source.emit_onset(id=request.args.get('id'), category=request.args.get('category'))
        return "ok"

    @app.route("/end")
    def end():
        event_source.emit_end()
        return "ok"

    @sockets.on("connect")
    def listen(ws):
        emit("status", {'data': 'connected'})

    def broadcast(data_type, stimulusId, stimulusCat):
        if stimulusId is not None:
            sockets.emit('data', {'data': {'type': data_type, 'stimulus': {'id': str(stimulusId), 'category': stimulusCat}}})
        else:
            sockets.emit('data', {'data': {'type': data_type}})

    # event_source.on("onset", broadcast)
    # event_source.on("unset", broadcast)
    # event_source.on("end", broadcast)

    def run_app(port):
        sockets.run(app, port=port)
    
    return run_app